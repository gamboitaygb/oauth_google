from django.shortcuts import render,redirect
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.helpers import credentials_from_session
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow, Flow
CLIENTE_CONFIG={"web":{"client_id":"411205432423-77hneevuc28krsoq3qk689mrgic8tbmj.apps.googleusercontent.com","project_id":"upload-file-to-drive-272614","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://oauth2.googleapis.com/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"ngL42ZZ-M84A4pfkohpA3n85","redirect_uris":["http://localhost:8000"],"javascript_origins":["http://localhost:8000"]}}
SCOPES = ['https://www.googleapis.com/auth/drive']
from requests_oauthlib import OAuth2Session


def index(request):

    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)

    if not 'code' in request.GET:
        session = OAuth2Session(
            client_id='411205432423-77hneevuc28krsoq3qk689mrgic8tbmj.apps.googleusercontent.com',
            scope=SCOPES,
            redirect_uri='http://localhost:8000'
        )
        url,_ = session.authorization_url(
            'https://accounts.google.com/o/oauth2/auth',
        )
        return redirect(url)

    code = request.GET['code']

    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            session = OAuth2Session(
                client_id='411205432423-77hneevuc28krsoq3qk689mrgic8tbmj.apps.googleusercontent.com',
                scope=SCOPES,
                redirect_uri='http://localhost:8000'
            )
            token = session.fetch_token(
                'https://oauth2.googleapis.com/token',
                code=code,
                include_client_id=True,
                client_secret='ngL42ZZ-M84A4pfkohpA3n85'
            )
            creds = credentials_from_session(session, CLIENTE_CONFIG)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)


    service = build('drive', 'v3', credentials=creds)
    
    results = service.files().list(
        pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])

    if not items:
        print("files not found")
    else:
        for item in items:
            print(u'{0} ({1})'.format(item['name'], item['id']))


    return render(request, 'index.html',{'code':code})


def upload(request):
    return render(request, 'upload.html')    
