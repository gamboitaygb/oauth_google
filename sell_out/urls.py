from django.urls import path
from sell_out.views import index , upload

urlpatterns = [
    path('',index, name='index'),
    path('upload-file',upload, name='upload')
]