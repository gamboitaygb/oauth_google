from django.apps import AppConfig


class SellOutConfig(AppConfig):
    name = 'sell_out'
